
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Uniform space structure on patterns, via the matching restrictions
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

Require Export Symbolic_Dynamics.Patterns.
Require Export Symbolic_Dynamics.Uniform.

Section Patterns_Uniform.

  Variable 𝕄 : Type.  (* the "time" domain, e.g. N^d or Z^d (d: nat) *)
  Variable 𝔸 : Set.  (* the alphabet, the space of the symbols,
                        the equality of symbols is proof-irrelevant *)

  Variable S : Symbolic_Dynamics.Families.Family (configuration 𝕄 𝔸).

  Let pj := @projT1 _ S.

  Inductive entourage_base (U: Symbolic_Dynamics.Families.Family 𝕄):
    Relation_Definitions.relation (sigT S) :=
  | entourage_base_intro: forall x y : sigT S,
      @restriction _ _ U _ always_in_full (pj x) =
      @restriction _ _ U _ always_in_full (pj y)
      -> entourage_base U x y.
  Inductive fundamental_system: Ensembles.Ensemble
                                  (Relation_Definitions.relation _) :=
  | fundamental_system_intro: forall U,
      fundamental_system (entourage_base U).

  Definition uniformity :=
    uniformity_from_fundamental_system fundamental_system.

  Lemma indeed_is_reflexive:
    is_reflexive _ uniformity.
  Proof.
    intros ? ? ?.
    destruct H.
    destruct H as [V ?].
    destruct H as [Hi Hf].
    destruct Hf.
    apply (Hi x x).
    constructor; reflexivity.
  Qed.

  Lemma indeed_is_upper_closed:
    is_upper_closed _ uniformity.
  Proof.
    intros ? ? HI ?.
    destruct H.
    destruct H.
    destruct H.
    constructor.
    exists x.
    split.
    + intros a b xab.
      apply HI.
      apply (H a b); assumption.
    + assumption.
  Qed.

  Lemma entourage_base_converse:
    forall U, entourage_base U = converse _ (entourage_base U).
  Proof.
    intros.
    apply extensionality_relations; split.
    all: intros ? ? ?.
    all: destruct H.
    1: constructor.
    2: destruct H.
    all: now constructor.
  Qed.

  Lemma indeed_is_stable_by_symmetry:
    is_stable_by_symmetry _ uniformity.
  Proof.
    intros ? ?.
    destruct H.
    destruct H.
    destruct H as [Hi Hf].
    constructor.
    exists (converse _ x).
    split.
    + intros a b cab.
      destruct cab as [? ? H].
      specialize (Hi a b H).
      constructor; assumption.
    + destruct Hf.
      rewrite <- entourage_base_converse.
      constructor.
  Qed.

  Import FunctionalExtensionality.

  Lemma intersection_of_entourage_bases_is_that_of_the_union:
    forall  U' V',
      intersection _ (entourage_base U') (entourage_base V')
      = entourage_base (Families.Union _ U' V').
  Proof.
    intros.
    apply extensionality_relations; split.
    - intros ? ? ?.
      destruct H as [? ? HU HV].
      constructor.
      apply functional_extensionality; intro.
      destruct x as [t U'V't].
      cbv.
      destruct U'V't as [t ?|t ?].
      1: destruct HU.
      2: destruct HV.
      1: assert (H'' := equal_f H (existT _ t u)); clear H.
      2: assert (H'' := equal_f H (existT _ t v)); clear H.
      all: cbv in H'' |- *.
      all: rewrite H''; reflexivity.
    - intros ? ? ?.
      destruct H.
      constructor.
      all: constructor.
      all: cbv in H |- *.
      all: apply functional_extensionality.
      1: intro u; destruct u as [t U't].
      2: intro v; destruct v as [t V't].
      1: pose (H' := equal_f H (existT _ t (Families.Union_intro_0
                                              _ _ _ _ U't))).
      2: pose (H' := equal_f H (existT _ t (Families.Union_intro_1
                                              _ _ _ _ V't))).
      all: cbv in H'.
      all: rewrite H'; reflexivity.
  Qed.

  Lemma indeed_is_stable_by_intersection:
    is_stable_by_intersection _ uniformity.
  Proof.
    intros ? ? HU HV.
    destruct HU as [U HU].
    destruct HV as [V HV].
    destruct HU as [U' HU].
    destruct HV as [V' HV].
    constructor.
    exists (intersection _ U' V').
    split.
    + intros ? ? ?.
      destruct H as [? ? HU' HV'].
      destruct HU as [HU _].
      destruct HV as [HV _].
      specialize (HU a b HU').
      specialize (HV a b HV').
      constructor. all: assumption.
    + destruct HU as [_ HfU].
      destruct HV as [_ HfV].
      destruct HfU as [U'].
      destruct HfV as [V'].
      rewrite intersection_of_entourage_bases_is_that_of_the_union.
      constructor.
  Qed.

  Lemma indeed_there_exist_square_roots:
    there_exist_square_roots _ uniformity.
  Proof.
    intros ? ?.
    unfold square_is_in.
    destruct H as [V H].
    destruct H as (V' & HIVU & HFSV).
    destruct HFSV.
    eexists.
    split.
    + constructor.
      exists (entourage_base U).
      split.
      * intros ? ? ?. apply H.
      * constructor.
    + intros ? ? ?.
      apply HIVU. clear V HIVU.
      destruct H.
      destruct H as (b & Ha & Hc).
      constructor.
      destruct Ha as [x y Hxy].
      rewrite Hxy.
      destruct Hc as [z ? Hxz].
      assumption.
  Qed.

  Lemma indeed_a_uniform_structure:
    is_uniform_structure _ uniformity.
  Proof.
    split. exact indeed_is_reflexive.
    split. exact indeed_is_upper_closed.
    split. exact indeed_is_stable_by_symmetry.
    split. exact indeed_is_stable_by_intersection.
    exact indeed_there_exist_square_roots.
  Qed.

End Patterns_Uniform.
