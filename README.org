
* Dependencies

  The topology library by Daniel Schepler:

  - https://github.com/coq-community/topology
  - https://github.com/coq-community/zorns-lemma

  These may be installed from OPAM by [fn:SE]:

  #+BEGIN_SRC shell
  opam repo add coq-released https://coq.inria.fr/opam/released
  opam update
  opam upgrade  # if possible, for Dune
  opam install coq-topology
  #+END_SRC

  Or (better) from the sources (see below).

* Building fresh version from VCS

  #+BEGIN_SRC shell
  git clone git@github.com:coq-community/zorns-lemma.git
  cd zorns-lemma
  make all
  cd ..
  #+END_SRC

  #+BEGIN_SRC shell
  git clone git@github.com:coq-community/topology.git
  cd topology
  sed -i '2s@^$@-R ../zorns-lemma ZornsLemma@' Make  # set the real path
  make all
  ln -s Make _CoqProject  # for ProofGeneral to see the real paths
  cd ..
  #+END_SRC

  #+BEGIN_SRC shell
  git clone git@gitlab.com:symbolic-dynamics/symbolic-dynamics.git
  cd symbolic-dynamics
  coq_makefile -f _CoqProject -o Makefile
  make all
  #+END_SRC

  The real paths for the dependencies are already specified in ~_CoqProject~ as above,
  change them, if you have a different directory hierarchy.

* Footnotes

[fn:SE] https://stackoverflow.com/questions/49602587/installing-packages-for-coq-using-opam
